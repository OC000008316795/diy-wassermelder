/*
 * SmartGreenCityHassfurt
 * Beispiel: DIY-Wassermelder Beispiel für Einsteiger 
 * MarkusW 
 * 18-04-2023
 * 
 */

//Deklaration
int messwert = 0; 		// Variable erstellen und nullen
#define BUZZER 6			// Buzzer Digital-OUT  Pin = ~D6
#define LED 7			// LED Digital-OUT Pin = D7
#define SENSOR_MESSWERT A0	// Analog-In am A0 /  Pin = D14

//SETUP
void setup() {
Serial.begin(9600);          // Kommunikation mit seriellen Port starten
pinMode (BUZZER,OUTPUT);     // Pin 6 für den Buzzer als Ausgang deklarieren
pinMode (LED,OUTPUT);        // Pin 7 für die LED als Ausgang deklarieren

}

//LOOP
void loop() {
messwert=analogRead(SENSOR_MESSWERT);     //Messwert einlesen
Serial.print("Feuchtigkeits-Meßwert: ");  //Serielle Ausgabe
Serial.println(messwert);
delay(100);                              	//Wartezeit 
  if (messwert > 200){          		//Beginn der Abfrage: Wenn der Sensorwert kleiner als 200 ist dann .. 
                                    //(Sensorwert max ist 800 = komplett im Wasser)
    digitalWrite(BUZZER, HIGH);  		//soll der Buzzer 
    digitalWrite(LED, HIGH);    		//und die LED eingeschaltet werden
  }
  else  {                       		//ansonsten
    digitalWrite(BUZZER, LOW);   		//sind beide aus.
    digitalWrite(LED, LOW); 
  }
}
