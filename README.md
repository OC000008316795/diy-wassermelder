# DIY-Wassermelder als autarke Einheit

![1](/bilder/4.jpg){ width=50% }

Unser Wassermelder ist eine autarke Einheit, die ähnlich wie ein Rauchmelder funktioniert. Sobald der Boden nass wird, gibt der Melder ein Signal ab, um Sie zu informieren, dass womöglich Wasser ausgelaufen ist. Dank der einfachen Umsetzung und der Einsteigerfreundlichkeit unseres Projekts können Sie den Wassermelder leicht in Betrieb nehmen. Sie können das Projekt mit einem Akkubetrieb oder einer Funkanbindung zu einem Smart Home System erweitern, wenn Sie möchten. Wenn Sie an einer Erweiterung des Projekts interessiert sind, helfen wir Ihnen gerne weiter und vertiefen das Thema.

Benötigte Teile:
- Arduino Nano
- Soil Moisture Sensor 
- Grove Buzzer 
- 220 Ohm Widerstand 
- 5mm Led 
- 3D-Druckteile für das Gehäuse (STL-Dateien stehen zum Download bereit)

Anleitung:
1. 3D-Druckteile 
2. Elektronik verdrahten und Zusammenbau 
3. Code 
4. Testen 
5. Abschluss


### 1. 3D-Druck
3D-Druckteile Lade zuerst die STL-Dateien für das Gehäuse herunter und drucke diese aus. Die Dateien enthalten alle benötigten Teile für das Gehäuse. Achte darauf, dass die Druckqualität ausreichend ist und die Teile sauber gedruckt sind. Entferne überschüssiges Material und säubere die Teile.



### 2. Elektronik

![1](/Elektrotechnik/hochwassermelder_Steckplatine.jpg){ width=50% }

![1](/Elektrotechnik/hochwassermelder_Schaltplan.jpg){ width=50% }

Verbinde den Soil Moisture Sensor mit dem Arduino Nano. Der Sensor verfügt über drei Anschlüsse: VCC, GND und SIG. Verbinde VCC mit 5V, GND mit GND und SIG mit einem freien Analogeingang (PIN A0).

Verbinde den Grove Buzzer mit dem Arduino Nano. Der Buzzer verfügt über drei Anschlüsse: GND, VCC und SIG. Verbinde GND mit GND, VCC mit 3,3V oder 5V und SIG mit einem freien Digitalausgang (PIN ~D6).

Verbinde die LED mit dem Arduino Nano. Verbinde den langen Anschluss (Anode) mit einem 220-Ohm-Widerstand und diesen mit einem freien Digitalausgang (PIN D7). Verbinde den kurzen Anschluss (Kathode) direkt mit GND.

![5](/bilder/5.jpg){ width=50% }

![2](/bilder/2.jpg){ width=50% }


### 3.Code
Lade den Arduino-Code für den Wassermelder herunter und öffne ihn in der Arduino-IDE. Passe den Code an deine Pinbelegung an, falls du andere Pins als in dieser Anleitung verwendest. Danach lade den Code auf den Arduino Nano hoch.
Der Code misst kontinuierlich die anliegende Feuchtigkeit und gibt ein akustisches Signal, wenn die Feuchtigkeit bestimmten Wert überschreitet. Gleichzeitig leuchtet die LED auf.

![1](/arduino/code.png){ width=50% }

### 4. Testen
Schalte den Wassermelder ein und teste, ob er wie erwartet funktioniert. Platziere den Sensor auf einen feuchten Schwamm und beobachte, wie das Signal ausgelöst wird.

### 5. Abschluss
Herzlichen Glückwunsch, du hast erfolgreich einen Wassermelder mit Arduino gebaut! 

![3](/bilder/3.jpg){ width=50% }
